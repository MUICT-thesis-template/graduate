\select@language {english}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bf {ACKNOWLEDGMENTS}}{iii}{section*.1}
\renewcommand {\@dotsep }{400}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bf {ABSTRACT (ENGLISH)}}{iv}{section*.2}
\renewcommand {\@dotsep }{400}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bf {ABSTRACT (THAI)}}{v}{section*.3}
\renewcommand {\@dotsep }{400}
\thispagestyle {fancy}
\fancyhead [L]{}
\fancyhead [R]{\header \thepage }
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bfseries LIST OF TABLES}{viii}{section*.5}
\renewcommand {\@dotsep }{400}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bfseries LIST OF FIGURES}{x}{section*.7}
\renewcommand {\@dotsep }{400}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{LIST OF CODED LISTINGS}{xii}{section*.9}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\nocolonnumberline {\bfseries CHAPTER\nobreakspace {}I} \bfseries \MakeUppercase {How to use \LaTeX \nobreakspace {} for your thesis?}}{1}{chapter.1}
\contentsline {section}{\nocolonnumberline {1.1}Whait is \LaTeX ?}{1}{section.1.1}
\contentsline {subsection}{\nocolonnumberline {1.1.1}subsection}{1}{subsection.1.1.1}
\contentsline {section}{\nocolonnumberline {1.2}\TeX \nobreakspace {} structure}{1}{section.1.2}
\contentsline {section}{\nocolonnumberline {1.3}Switching the language}{2}{section.1.3}
\contentsline {section}{\nocolonnumberline {1.4}Generating ICT thesis}{3}{section.1.4}
\contentsline {section}{\nocolonnumberline {1.5}Using thesis.tex}{4}{section.1.5}
\contentsline {section}{\nocolonnumberline {1.6}Generating the page styles}{6}{section.1.6}
\contentsline {chapter}{\nocolonnumberline {\bfseries CHAPTER\nobreakspace {}II} \bfseries \MakeUppercase {\LaTeX 's\nobreakspace {}Arsenal}}{9}{chapter.2}
\contentsline {section}{\nocolonnumberline {2.1}Sectioning}{9}{section.2.1}
\contentsline {section}{\nocolonnumberline {2.2}Paragraph}{20}{section.2.2}
\contentsline {section}{\nocolonnumberline {2.3}Paragraph alignment}{20}{section.2.3}
\contentsline {subsection}{\nocolonnumberline {2.3.1}Indentation Indentation Indentation Indentation Indentation Indentation Indentation }{21}{subsection.2.3.1}
\contentsline {subsection}{\nocolonnumberline {2.3.2}Format alignment}{21}{subsection.2.3.2}
\contentsline {section}{\nocolonnumberline {2.4}Bullets and Numbering}{22}{section.2.4}
\contentsline {section}{\nocolonnumberline {2.5}Figure}{24}{section.2.5}
\contentsline {section}{\nocolonnumberline {2.6}Table}{26}{section.2.6}
\contentsline {section}{\nocolonnumberline {2.7}Page sectioning}{30}{section.2.7}
\contentsline {section}{\nocolonnumberline {2.8}Landscape mode}{30}{section.2.8}
\contentsline {section}{\nocolonnumberline {2.9}Text style, type, and size}{33}{section.2.9}
\contentsline {subsection}{\nocolonnumberline {2.9.1}Text style}{33}{subsection.2.9.1}
\contentsline {subsection}{\nocolonnumberline {2.9.2}Text size}{33}{subsection.2.9.2}
\contentsline {section}{\nocolonnumberline {2.10}Mathematic Formular}{34}{section.2.10}
\contentsline {subsection}{\nocolonnumberline {2.10.1}Numbering the multiple equations}{35}{subsection.2.10.1}
\contentsline {subsection}{\nocolonnumberline {2.10.2}Subscript and Superscript}{36}{subsection.2.10.2}
\contentsline {subsection}{\nocolonnumberline {2.10.3}Spacing in math mode}{37}{subsection.2.10.3}
\contentsline {subsection}{\nocolonnumberline {2.10.4}Adjust delimiter size}{37}{subsection.2.10.4}
\contentsline {section}{\nocolonnumberline {2.11}Adding footnotesSometimes the extra content is necessary to provide more readability,}{38}{section.2.11}
\contentsline {chapter}{\nocolonnumberline {\bfseries CHAPTER\nobreakspace {}III} \bfseries \MakeUppercase {Sriting bib extension fileWriting bib extension fileWriting bib extension file}}{40}{chapter.3}
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bfseries REFERENCES}{43}{lstnumber.3.2.24}
\contentsline {chapter}{\bfseries {APPENDICES}}{45}{lstnumber.3.2.24}
\setcounter {tocdepth}{1}
\renewcommand {\@dotsep }{400}
\@inappendixtrue 
\contentsline {section}{Appendix A\hspace {.7em}\LaTeX \nobreakspace {} Mathematic symbol and font styleMathematic symbol and font style}{46}{appendix.1}
\@inappendixfalse 
\setcounter {tocdepth}{1}
\renewcommand {\@dotsep }{400}
\@inappendixtrue 
\contentsline {section}{Appendix B\hspace {.7em}Refernce types in BibTeX }{50}{appendix.2}
\@inappendixfalse 
\setcounter {tocdepth}{0}
\renewcommand {\@dotsep }{400}
\contentsline {chapter}{\bfseries BIOGRAPHY}{55}{lstnumber.B.9.10}
