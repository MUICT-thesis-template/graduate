\thispagestyle {fancy}
\contentsline {lstlisting}{\numberline {1.1}\TeX \nobreakspace {}structure}{1}{lstlisting.1.1}
\contentsline {lstlisting}{\numberline {1.2}\TeX \nobreakspace {}file header}{4}{lstlisting.1.2}
\contentsline {lstlisting}{\numberline {1.3}Author's information}{5}{lstlisting.1.3}
\contentsline {lstlisting}{\numberline {1.4}Advisor's information}{5}{lstlisting.1.4}
\contentsline {lstlisting}{\numberline {1.5}Thesis's information}{6}{lstlisting.1.5}
\contentsline {lstlisting}{\numberline {1.6}Page generator command}{7}{lstlisting.1.6}
\contentsline {lstlisting}{\numberline {1.7}Call the external file via \textbackslash input}{7}{lstlisting.1.7}
\contentsline {lstlisting}{\numberline {2.1}Sectioning}{9}{lstlisting.2.1}
\contentsline {lstlisting}{\numberline {2.2}Extra spaces between paragraphs}{20}{lstlisting.2.2}
\contentsline {lstlisting}{\numberline {2.3}Indent spaces}{21}{lstlisting.2.3}
\contentsline {lstlisting}{\numberline {2.4}Using itemize and enumerate}{23}{lstlisting.2.4}
\contentsline {lstlisting}{\numberline {2.5}Using itemize and enumerate}{23}{lstlisting.2.5}
\contentsline {lstlisting}{\numberline {2.6}The figure environment structure}{24}{lstlisting.2.6}
\contentsline {lstlisting}{\numberline {2.7}Using subcaption package}{25}{lstlisting.2.7}
\contentsline {lstlisting}{\numberline {2.8}Create the table}{27}{lstlisting.2.8}
\contentsline {lstlisting}{\numberline {2.9}Minipage example}{30}{lstlisting.2.9}
\contentsline {lstlisting}{\numberline {2.10}Enable landscape mode}{31}{lstlisting.2.10}
\contentsline {lstlisting}{\numberline {2.11}Command for setting font environment}{33}{lstlisting.2.11}
\contentsline {lstlisting}{\numberline {2.12}Formular environment declaration}{34}{lstlisting.2.12}
\contentsline {lstlisting}{\numberline {2.13}Multiple equations}{35}{lstlisting.2.13}
\contentsline {lstlisting}{\numberline {2.14}Create blank space in math environment}{37}{lstlisting.2.14}
\contentsline {lstlisting}{\numberline {2.15}The differences of delimiter size when using and not using the \textbackslash left \textbackslash right command.}{38}{lstlisting.2.15}
\contentsline {lstlisting}{\numberline {2.16}Adding footnote}{39}{lstlisting.2.16}
\contentsline {lstlisting}{\numberline {3.1}Define the reference format}{40}{lstlisting.3.1}
\contentsline {lstlisting}{\numberline {3.2}BibTex data}{41}{lstlisting.3.2}
\contentsline {lstlisting}{\numberline {B.1}Article}{50}{lstlisting.3.1}
\contentsline {lstlisting}{\numberline {B.2}Unpublished document}{50}{lstlisting.3.2}
\contentsline {lstlisting}{\numberline {B.3}Inproceeding paper}{51}{lstlisting.3.3}
\contentsline {lstlisting}{\numberline {B.4}Proceeding paper}{51}{lstlisting.3.4}
\contentsline {lstlisting}{\numberline {B.5}Technical report}{52}{lstlisting.3.5}
\contentsline {lstlisting}{\numberline {B.6}Website}{52}{lstlisting.3.6}
\contentsline {lstlisting}{\numberline {B.7}PhD. and Master's thesis}{53}{lstlisting.3.7}
\contentsline {lstlisting}{\numberline {B.8}Book}{53}{lstlisting.3.8}
\contentsline {lstlisting}{\numberline {B.9}Miscellaneous}{54}{lstlisting.3.9}
