%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%
% MUICT Thesis Style
%
% Normally there is no need to edit this file. It should be called, using
% \usepackage{muictthesis.sty}, from thesis.tex.  
%
% Version: 1.2
% Author: Karin Sumongkayothin 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


\NeedsTeXFormat{LaTeX2e}[1998/12/01]
\ProvidesPackage{muictthesis}[2014/05/12 v0.1 MU Bachelor Thesis Style]

% Required packages
\RequirePackage{indentfirst}
\RequirePackage{fontspec}
\RequirePackage{xunicode}
\RequirePackage{xltxtra}
\RequirePackage{polyglossia}
\RequirePackage{setspace}
\RequirePackage {fancybox}
\RequirePackage{fancyhdr}
\RequirePackage{showframe}
\RequirePackage{lastpage}
% For automatic switching between languages
\RequirePackage[Latin,Thai]{ucharclasses}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%English thesis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setmainlanguage{english}
\setotherlanguages{thai}

% Set environment for Thai fonts
\newenvironment{thailang}{\thaifont}{}

% Create a compatibility to the old fontspec version  
\ifx\undefinded\setfontfamily
	\let\setfontfamily\newfontfamily
\fi

%Create a compatibility to Thai URL
\renewcommand{\url}[1]{\href{#1}{#1}}


% Mahidol uses Times New Roman(12pt) for English and Angsana New(16pt) for Thai  
\setmainfont[Ligatures=TeX, SizeFeatures={Size=12}]{Times New Roman}
\newfontfamily{\thaifont}[Script=Thai, SizeFeatures={Size=16}]{Angsana New}
\newfontfamily{\header}[SizeFeatures={Size=10}]{Times New Roman}
% When using Thai characters use thailang environment then switch back to the original environment
\setTransitionsFor{Thai}{\begin{thailang}}{\end{thailang}}

%Font size
\setfontfamily{\Huge}[SizeFeatures={Size=24.88}]{Times New Roman}
\setfontfamily{\huge}[SizeFeatures={Size=24.88}]{Times New Roman}
\setfontfamily{\LARGE}[SizeFeatures={Size=20.74}]{Times New Roman}
\setfontfamily{\Large}[SizeFeatures={Size=17.28}]{Times New Roman}
\setfontfamily{\large}[SizeFeatures={Size=14.4}]{Times New Roman}
\setfontfamily{\normalsize}[SizeFeatures={Size=12}]{Times New Roman}
\setfontfamily{\small}[SizeFeatures={Size=10.95}]{Times New Roman}
\setfontfamily{\footnotesize}[SizeFeatures={Size=10}]{Times New Roman}
\setfontfamily{\scriptsize}[SizeFeatures={Size=8}]{Times New Roman}
\setfontfamily{\tiny}[SizeFeatures={Size=6}]{Times New Roman}
\newcommand{\titlefont}[1]{\textbf{\fontsize{14}{17}\selectfont #1}} 
\newcommand{\titlefontTH}[1]{\textbf{\fontsize{22}{21.6}\selectfont #1}}

%=============================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Global Page layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\parindent}{1.27cm}	% 1.27cm indent
\renewcommand{\baselinestretch}{1.5}

\setlength{\hoffset}{0in}		%h mark = 1inch 
\setlength{\voffset}{0in}		%v mark = 1inch
%
\setlength{\oddsidemargin}{1.2cm}  % 3.75cm left margin
\setlength{\evensidemargin}{1.21cm} % 3.75cm left margin (even pages)
\setlength{\marginparsep}{0.2cm}
%\setlength{\marginparwidth}{0in}
%
%
\setlength{\topmargin}{-0.04cm}       % 2.5cm header margin
\setlength{\headsep}{0.75cm}        % 3.75cm top margin
%
%\setlength{\footskip}{-2.5cm}
\setlength{\textwidth}{14.75cm}        
\setlength{\textheight}{23.45cm}        
\flushbottom 


%test header style
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}	% Remove header horizontal line
%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Cover Page layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\coverpage}{
\frontmatter
\thispagestyle{empty}
\begin{center}
%	\vspace*{0.5\baselineskip}
	\titlefont{
	\MakeUppercase{\thesisTitle}\\
	\titlefontTH{\thesisTitleTH}\\
		\begin{spacing}{1}
			\titlefont{
				\vfill
				\vspace*{.25\baselineskip}
				\makeatletter% Start TeX commands
					\MakeUppercase{\AuthorTitleName~\AuthorName}
				\makeatother % End TeX commands
					\vspace*{3\baselineskip}\\
				\vfill
				A THESIS SUBMITTED IN PARTIAL FULFILLMENT\\
				OF THE REQUIREMENTS FOR THE DEGREE OF\\
				\MakeUppercase{\Degree}~\MakeUppercase{(\SubDegree)}\\
				\MakeUppercase{\Department}\\
				\MakeUppercase{\University}\\
				\AcademicYear
				\vspace*{2\baselineskip}\\
				COPYRIGHT OF MAHIDOL UNIVERSITY
				\vspace*{-1\baselineskip}
			}
		\end{spacing}
	}
\end{center}
\clearpage
\setcounter{page}{1}
}
%===========================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Entitled page layout I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\@sigdots{..........................................................}
\newcommand{\@sbx}[1]{\parbox[t]{70mm}{\raggedright\@sigdots\\#1}}
\newcommand{\entitlepage}{
\thispagestyle{empty}
\begin{spacing}{1}
\begin{center}
	Thesis\\
	entitled\\
	\titlefont{\MakeUppercase{\thesisTitle}}\\
\end{center}%
\vfill
\begin{tabbing} 
	\hspace{0.53\textwidth}\=\@sbx{\AuthorTitleName~\AuthorName\\
	Candidate} \\ \\ \\ 
	\hspace{0.53\textwidth}\@sbx{\AdvTitle~\Adv\\\AdvDegree~(\AdvDegreeArea)\\
	Major advisor} \\ \\ \\
	\ifx\undefinded\CoAdvI {}\else \hspace{0.53\textwidth}\@sbx{\CoAdvITitle~\CoAdvI\\\CoAdvIDegree~(\CoAdvIDegreeArea)\\Co-advisor}\\ \\ \\ \fi 
	\ifx\undefinded\CoAdvII{}\else \hspace{0.53\textwidth}\@sbx{\CoAdvIITitle~\CoAdvII\\\CoAdvIIDegree~(\CoAdvIIDegreeArea)\\Co-advisor}\fi 
	\ifx\undefinded\CoAdvIII{}\else \hspace{0.53\textwidth}\@sbx{\CoAdvIIITitle~\CoAdvIII\\\CoAdvIIIDegree~(\CoAdvIIIDegreeArea)\\Co-advisor} \\ \\ \\ \fi 
	\@sbx{\GraduateDeanTitle~\GraduateDean\\
	\GraduateDeanDegree~(\GraduateDeanDegreeArea)\\
	Dean\\Faculty of Graduate Studies \\Mahidol University}
	\>\@sbx{\ProgramDirTitle~\ProgramDir, \\\ProgramDirDegree~(\ProgramDirDegreeArea) \\Program Dirtector \\
	\Degree~Program\\in \SubDegree\\ \Faculty\\ \University\\}
\end{tabbing}
\end{spacing}
\clearpage
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Entitled page layout II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\thispagestyle{empty}
\begin{spacing}{1}
\begin{center}
	Thesis\\
	entitled\\
	\titlefont{\MakeUppercase{\thesisTitle}}\\
	\vspace*{1\baselineskip}
	was submitted to the Faculty of Graduate Studies, \University\\
	for the degree of \Degree~(\SubDegree)\\
	on\\
	\GradMonth~\GradDay,~\GradYear	
\end{center}%
\vfill
\begin{tabbing} 
	\hspace{0.53\textwidth}\=\@sbx{\AuthorTitleName~\AuthorName\\
	Candidate} \\ \\ \\ 
	\hspace{0.53\textwidth}\=\@sbx{\Chair\\\ChairDegree~(\ChairDegreeArea)\\
	Chair} \\ \\ \\
	\ifx\undefinded\MemberI \hspace{0.53\textwidth} \else \@sbx{\MemberITitle~\MemberI\\\MemberIDegree~(\MemberIDegreeArea)\\Member}\> \fi 
	\ifx\undefinded\MemberII{}\else \@sbx{\MemberIITitle~\MemberII\\\MemberIIDegree~(\MemberIIDegreeArea)\\Member}\\ \\ \\ \fi 
	\ifx\undefinded\MemberIII \hspace{0.53\textwidth} \else \@sbx{\MemberIIITitle~\MemberIII\\\MemberIIIDegree~(\MemberIIIDegreeArea)\\Member}\> \fi 
	\ifx\undefinded\MemberIV{}\else \@sbx{\MemberIVTitle~\MemberIV\\\MemberIVDegree~(\MemberIVDegreeArea)\\Member}\\ \\ \\ \fi 
	\@sbx{\GraduateDeanTitle~\GraduateDean\\
	\GraduateDeanDegree~(\GraduateDeanDegreeArea)\\
	Dean\\Faculty of Graduate Studies \\Mahidol University}
	\>\@sbx{\FactDeanTitle~\FactDean, \\\FactDeanDegree~(\FactDeanDegreeArea) \\
	Dean\\\Faculty \\\University\\
	}
\end{tabbing}
\end{spacing}
\clearpage
}

%===========================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Acknowledgement layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newenvironment{acknowledgement}
{
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{\bf{ACKNOWLEDGMENTS}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\fancyhead[R]{\header\thepage}
	\fancyfoot{}
	\begin{center}
		\vspace*{0.5\baselineskip}
		\titlefont{\MakeUppercase{Acknowledgements}}
		\vspace*{1.5\baselineskip}
	\end{center}%
	\par
}
{%
	\par
	\vspace*{\baselineskip}
	\hfill\AuthorName\par
	\clearpage
}

%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%English Abstract layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\@kwbx}[1]{\parbox[t]{.82\textwidth}{#1}}

\newenvironment{abstractENG}{%
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{\bf{ABSTRACT (ENGLISH)}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}

	\fancyhead[L]{\header{Fac. of Grad. Studies, Mahidol Univ.}}
	\fancyhead[R]{\header{Thesis / \thepage}}
	\fancyfoot{}
	\fancyput(1cm,-25cm){%
		\setlength{\unitlength}{1cm}%
		\framebox(15.15,23.85){}%
	}
	\begin{spacing}{1}
	\noindent	
	\MakeUppercase{\thesisTitle}\\ 
	\vspace*{\baselineskip}\\ 
	\MakeUppercase{\AuthorName}~~\AuthorID~ITCS/\ifthenelse{\equal{\Degree}{Master of Science}}{M}{D}\\
	\vspace*{\baselineskip}\\ 
	\ifx\Degree\master{M.Sc.}\else{Ph.D.}\fi~(\MakeUppercase{\SubDegree})\\
	\vspace*{\baselineskip}\\
	THESIS ADVISORY COMMITTEE: \MakeUppercase{\Adv},~\AdvDegree\ifx\undefinded\CoAdvI{}\else{,~\MakeUppercase{\CoAdvI},~\CoAdvIDegree}\fi\ifx\undefinded\CoAdvII{}\else{,~\MakeUppercase{\CoAdvII},~\CoAdvIIDegree}\fi\ifx\undefinded\CoAdvIII{}\else{,~\MakeUppercase{\CoAdvIII},~\CoAdvIIIDegree}\fi
	\vspace*{\baselineskip} 
	\begin{center}
		ABSTRACT	
	\end{center}%
	\par
	\end{spacing}
}
{	
	\par
	\vspace*{\baselineskip} 
	\noindent
			KEYWORDS:~\@kwbx{\MakeUppercase\thesisKeywords}\\ \\ \\
	\pageref{LastPage}~pages
	\clearpage
}

%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Thai Abstract layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{abstractTH}{%
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{\bf{ABSTRACT (THAI)}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}

	\fancyhead[L]{\header{Fac. of Grad. Studies, Mahidol Univ.}}
	\fancyhead[R]{\header{Thesis / \thepage}}
	\fancyfoot{}
	\fancyput(1cm,-25cm){%
		\setlength{\unitlength}{1cm}%
		\framebox(15.15,23.85){}%
	}
	\begin{spacing}{1.5}
		\noindent
		\thesisTitleTH\\
		\MakeUppercase\thesisTitle\\ \\
		\AuthorTitleNameTH~\AuthorNameTH~~\AuthorID~ITCS/\ifthenelse{\equal{\Degree}{Master of Science}}{M}{D}\\ \\
		\ifthenelse{\equal{\Degree}{Master of Science}}{วท.ม.}{ปร.ด.}~(\SubDegreeTH)\\ \\
คณะกรรมการที่ปรึกษาวิทยานิพนธ์: \AdvTH,~\AdvDegree\ifx\undefinded\CoAdvI{}\else{,~\CoAdvITH,~\CoAdvIDegree}\fi\ifx\undefinded\CoAdvII{}\else{,~\CoAdvIITH,~\CoAdvIIDegree}\fi\ifx\undefinded\CoAdvIII{}\else{,~\CoAdvIIITH,~\CoAdvIIIDegree}\fi\\ 
		\vspace{-.5\baselineskip}
		\begin{center}
			บทคัดย่อ	
		\end{center}%
		\vspace{-.5\baselineskip}
		\par
}
{	
		\noindent\\ \\
		\pageref{LastPage}~หน้า
	\end{spacing}
	\clearpage
}

%===========================================================

\makeatletter% Start TeX commands

\let\@afterindentfalse\@afterindenttrue
\@afterindenttrue


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%			Appendices layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\newcounter{appendix}		%create \c@appendix, theappendix
%\renewcommand \theappendix{\@Alph\c@appendix}
%\renewcommand{\appendix}[1]{%
%	\setcounter{figure}{0}
%	\setcounter{table}{0}
%	\setcounter{lstlisting}{0}
%	\clearpage
%	\setcounter{section}{\z@}
%	\renewcommand \thefigure {\theappendix.\@arabic\c@figure}	% change dot to some other symbols for changing to caption style 
%	\renewcommand \thetable {\theappendix.\@arabic\c@table}
%	\renewcommand \thesection {\theappendix.\@arabic\c@section}
%	\renewcommand \thelstlisting {\theappendix.\@arabic\c@lstlisting}
%	\refstepcounter{appendix}	
%	\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
%	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
%	\addcontentsline{toc}{chapter}{APPENDIX\space\theappendix}
%	\chaptermark{\header{Appendix\space\theappendix}}%	Appendix's header
%%	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{0.5}}
%	\begin{center}
%  		\vspace*{17\p@}%
%		\titlefont{APPENDIX~\theappendix}\par
%		\titlefont{\MakeUppercase{#1}}\par
%  	\vskip 30\p@
%	\end{center}%
%	\par
%}%
%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%					Biography Layout	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\biotable}[1]{%
	\begin{tabular}{@{}p{.45\textwidth}p{.55\textwidth}}
		\bfseries NAME					&\csname thesis#1TitleName\endcsname\space\csname thesis#1AuthorName\endcsname\space\csname thesis#1AuthorLastName\endcsname\\
		\bfseries DATE OF BIRTH			&\csname thesis#1AuthorBD\endcsname\\
		\bfseries PLACE OF BIRTH		&\csname thesis#1AuthorBP\endcsname\\
		\bfseries INSTITUTIONS ATTENDED	&\csname thesis#1AuthorHC\endcsname:\par \hspace{1.2cm}High School Diploma \par Mahidol University, \GradYear:\par\hspace{1.2cm}Bachelor of Science (ICT)\\
	\end{tabular}
	\vskip 15\p@
}

\newcommand{\biography}{%
		\fancyhead[L]{\ifthenelse{\isodd{\value{page}}}{\header{Fac. of Grad. Studies, Mahidol Univ.}}{\header\thesisAuthorsInitials}} %odd page header
		\fancyhead[R]{\ifthenelse{\isodd{\value{page}}}{\header{\DegreeAbb~(\SubDegree)~/~\thepage}}{\leftmark~/~\header\thepage}} %even page header
	\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\addcontentsline{toc}{chapter}{\bfseries BIOGRAPHY}
	\chaptermark{Biographies}%	Biography's header
	\begin{center}
  		\vspace*{17\p@}%
		\titlefont{\MakeUppercase{Biographies}}\par
    	\vskip 30\p@
			\biotable{First}
			\ifx\thesisSecondAuthorName\undefined
				{}
			\else
				\biotable{Second}
			\fi
			\ifx\thesisThirdAuthorName\undefined
				{}
			\else
				\biotable{Third}
			\fi
	\end{center}%
	\par
}%
%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%					Reference Layout	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%===========================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Table of Content, List of Figure, List of Table layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\renewcommand{\@dotsep}{400}	%Width between dots
\setcounter{tocdepth}{2}% TOC includes three depth (1., 1.1, 1.1.1)


%\newif\if@nodotsline	%declare boolean @istest with default value is false 
%\@nodotslinetrue		% => set @nodotsline to be true

\renewcommand\thefigure{\arabic{chapter}.\arabic{figure}} %Change figure numbering style
\renewcommand\thetable{\arabic{chapter}.\arabic{table}}	%Change table numbering style
%---------------------------------------------------
%			 ToC page layout
%---------------------------------------------------
\renewcommand\tableofcontents{%		
	%====== Add fancyhdr script to .toc file ==========
	\addtocontents{toc}{\protect\thispagestyle{fancy}}
	\addtocontents{toc}{\protect\fancyhead[L]{}}
	\addtocontents{toc}{\protect\fancyhead[R]{\header\protect\thepage}}
	%====== Add fancyhdr script to .toc file (End) ==========
	\vspace*{.5\baselineskip}
	\chapter*{CONTENTS}
	\hfill {\bfseries Page}
	\vskip \p@
	\@starttoc{toc}%
}

\newcommand{\nocolonnumberline}[1]{\hb@xt@\@tempdima{#1~\hfil}} 	%normal numberline 
\renewcommand{\numberline}[1]{\hb@xt@\@tempdima{#1:~\hfil}}			%numberline wittout colon 

%---------------------------------------------------
%	 	TOC Chapter list and Chapter layout format	
%---------------------------------------------------
\renewcommand*\thechapter{\Roman{chapter}}	%Change chapter numbering format
\renewcommand\chapter{%
	\secdef\@chapter\@schapter
}

\def\@chapter[#1]#2{%
	\ifnum \c@secnumdepth >\m@ne
		\if@mainmatter
			\refstepcounter{chapter}%
			\addcontentsline{toc}{chapter}%
			{\protect\nocolonnumberline{\bfseries CHAPTER~\thechapter}\bfseries\texorpdfstring{\MakeUppercase{ #1}}{}}%
		\else
			\addcontentsline{toc}{chapter}{#1}%
		\fi
	\else
		\addcontentsline{toc}{chapter}{#1}%
	\fi
	\chaptermark{#1}%	Chapter's header
%	\addtocontents{lof}{\protect\addvspace{10\p@}}%		Remove the comments if extra space between chapters is required
%	\addtocontents{lot}{\protect\addvspace{10\p@}}%
	\if@twocolumn
		\@topnewpage[\@makechapterhead{#2}]%
	\else
		\@makechapterhead{#2}%
		\@afterheading
	\fi}

%\def bold dotted line for chapter in TOC
\def\@chapdottedtocline#1#2#3#4#5{%
	\ifnum #1>\c@tocdepth \else
    	\vskip \z@ \@plus.2\p@
    	{\leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
     	\parindent #2\relax\@afterindenttrue
     	\interlinepenalty\@M
     	\leavevmode
     	\@tempdima #3\relax
     	\advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
     	{#4}\nobreak
     	\leaders\hbox{$\m@th
        	\mkern \@dotsep mu\hbox{\bfseries .}\mkern \@dotsep
        	mu$}\hfill
     	\nobreak
     	\hb@xt@\@pnumwidth{\hfil\normalfont \normalcolor #5}%
     	\par}%
  	\fi}

\renewcommand*\l@chapter[2]{%
	    \addpenalty{-\@highpenalty}%
		\setlength{\@tempdima}{6.7em}
	    \begingroup
	       \parindent \z@ \rightskip \@pnumwidth
	       \parfillskip -\@pnumwidth
	       \leavevmode
	       \hskip -\leftskip
		   #1\nobreak\hfil\nobreak
     	   \leaders\hbox{$\m@th
           \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
           mu$}\hfill
		   \hb@xt@\@pnumwidth{\hss\bfseries #2}\par		%Change toc chapter page numbering style
	       \penalty\@highpenalty
	    \endgroup
}

%---------------------------------------------------
%	 	TOC Section list and Section layout format	
%---------------------------------------------------
\renewcommand\thesection{\arabic{chapter}.\arabic{section}} %Change section numbering format
\renewcommand\section{\@afterindentfalse \secdef\@section\@ssection}

\def\@makesectionhead#1{ \vspace{16pt} { \par\parindent \z@ %\centering
	\Large\bf\baselineskip 12pt \thesection\hskip 0.7em #1\par
    \nobreak \vskip 8pt} }

\def\@makessectionhead#1{ \vspace{16pt} { \par\parindent \z@ %\centering
    \Large\bf\baselineskip 12pt #1\par
    \nobreak \vskip 8pt} }


\def\@section[#1]#2{\ifnum \c@secnumdepth >\m@ne
	\refstepcounter{section}
  	\addcontentsline{toc}{section}{\protect
	\nocolonnumberline{\thesection}\texorpdfstring{\MakeUppercase{#1}}{}}\else
	\addcontentsline{toc}{section}{\texorpdfstring{\MakeUppercase{#1}}{}}\fi
    \sectionmark{#1}
    \if@twocolumn\@topnewpage[\@makesectionhead{#2}]
	\else \@makesectionhead{#2}\@afterheading \fi
	}

\def\@ssection#1{\if@twocolumn \@topnewpage[\@makessectionhead{#1}]
    \else \@makessectionhead{#1}\@afterheading\fi}

%---------------------------------------------------
% 	TOC Subsection list and Subsection layout format	
%---------------------------------------------------
\renewcommand\subsection{\@afterindentfalse \secdef\@subsection\@ssubsection}

\def\@makesubsectionhead#1{ \vspace{16pt} { \par\parindent 5.5ex 
	\normalsize\bf\baselineskip 12pt \thesubsection\hskip 0.7em #1\par
    \nobreak \vskip 8pt} }

\def\@makessubsectionhead#1{ \vspace{16pt} { \par\parindent 5.5ex
	\normalsize\bf\ #1\par
    \nobreak \vskip 8pt} }

\def\@subsection[#1]#2{\ifnum \c@secnumdepth >\m@ne
	\refstepcounter{subsection}
  	\addcontentsline{toc}{subsection}{\protect
	\nocolonnumberline{\thesubsection}\texorpdfstring{\MakeUppercase{#1}}{}}\else
	\addcontentsline{toc}{subsection}{\texorpdfstring{\MakeUppercase{#1}}{}}\fi
 	\if@twocolumn\@topnewpage[\@makesubsectionhead{#2}]
  	\else \@makesubsectionhead{#2}\@afterheading \fi}

\def\@ssubsection#1{\if@twocolumn \@topnewpage[\@makessubsectionhead{#1}]
	\else \@makessubsectionhead{#1}\@afterheading\fi}

%---------------------------------------------------
%	TOC Subsubsection list and Subsubsection layout format	
%---------------------------------------------------

%\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}{-13pt plus
%	-1pt minus -.5pt}{1sp}{\normalsize\it}}
%
%\def\@makesubsubsectionhead#1{ \vsecspace{12pt} { \parindent 0pt \raggedright
%	\normalsize\rm \thesubsubsection.\hskip 0.7em \it #1\par
%	\nobreak \vskip 1sp} }
%
%\def\@makessubsubsectionhead#1{ \vsecspace{12pt} { \parindent 0pt \raggedright
%	\normalsize\it #1\par
%    \nobreak \vskip 1sp} }
%
%\def\subsubsection{\@afterindentfalse \secdef\@subsubsection\@ssubsubsection}
%
%\def\@subsubsection[#1]#2{\ifnum \c@secnumdepth >\m@ne
%	\refstepcounter{subsubsection}
%	\addcontentsline{toc}{subsubsection}{\protect
%	\numberline{\thesubsubsection}#1}\else
%    \addcontentsline{toc}{subsubsection}{#1}\fi
%    \if@twocolumn\@topnewpage[\@makesubsubsectionhead{#2}]
%    \else \@makesubsubsectionhead{#2}\@afterheading \fi}
%
%\def\@ssubsubsection#1{\if@twocolumn \@topnewpage[\@makessubsubsectionhead{#1}]
%	\else \@makessubsubsectionhead{#1}\@afterheading\fi}


\renewcommand{\subsubsection}[1]{\@startsection{subsubsection}{3}{\z@}%
	{-1.5ex \@plus -1ex \@minus -.2ex}%
    {0.8ex \@plus.2ex}%
	{\bfseries\leftskip \parindent}[\texorpdfstring{\MakeUppercase{#1}}{}]{\bfseries#1}}



\renewcommand*\l@section{\@dottedtocline{1}{3em}{1cm}}
\renewcommand*\l@subsection{\@dottedtocline{2}{5.4em}{1.3cm}}

%---------------------------------------------------
%	 	 LoF page layout	
%---------------------------------------------------
\renewcommand\listoffigures{%
%	\addtocontents{lof}{\protect\thispagestyle{fancy}}
%	\addtocontents{lof}{\protect\fancyhead[L]{}}
%	\addtocontents{lof}{\protect\fancyhead[R]{\protect\thepage}}
	\addtocontents{lof}{\protect\renewcommand{\protect\@dotsep}{400}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{LIST OF FIGURES}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\chapter*{LIST OF FIGURES}
	\vskip -22\p@  
	\hfill Page
	\vskip 22\p@
	\@starttoc{lof}%
}

\renewcommand*\l@figure[2]{%
	    \addpenalty{-\@highpenalty}%
		\setlength{\@tempdima}{2.5em}
	    \begingroup
	       \parindent \z@ \rightskip \@pnumwidth
	       \parfillskip -\@pnumwidth
	       \leavevmode
	       \advance\leftskip 5.4em% NEW: comment out if no indentation required for lines 2,3,...
	       \hskip -\leftskip
		   \figurename~#1\nobreak\hfil\nobreak
     	   \leaders\hbox{$\m@th
           \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
           mu$}\hfill
		   \hb@xt@\@pnumwidth{\hss #2}\par
	       \penalty\@highpenalty
	    \endgroup
}

%---------------------------------------------------
%	 	 LoT page layout	
%---------------------------------------------------
\renewcommand\listoftables{%
%	\addtocontents{lot}{\protect\thispagestyle{fancy}}
%	\addtocontents{lot}{\protect\fancyhead[L]{}}
%	\addtocontents{lot}{\protect\fancyhead[R]{\protect\thepage}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{LIST OF TABLES}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\chapter*{LIST OF TABLES}
	\vskip -22\p@  
	\hfill Page
	\vskip 22\p@
	\@starttoc{lot}%
}

\renewcommand*\l@table[2]{%
	    \addpenalty{-\@highpenalty}%
		\setlength{\@tempdima}{2.5em}
	    \begingroup
	       \parindent \z@ \rightskip \@pnumwidth
	       \parfillskip -\@pnumwidth
	       \leavevmode
	       \advance\leftskip 5.4em% NEW: comment out if no indentation required for lines 2,3,...
	       \hskip -\leftskip
		   \tablename~#1\nobreak\hfil\nobreak
     	   \leaders\hbox{$\m@th
           \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
           mu$}\hfill
		   \hb@xt@\@pnumwidth{\hss #2}\par
	       \penalty\@highpenalty
	    \endgroup
}


%---------------------------------------------------
%	 	 LoL page layout	
%---------------------------------------------------
\renewcommand\lstlistoflistings{%
%	\addtocontents{lol}{\protect\thispagestyle{fancy}}
%	\addtocontents{lol}{\protect\fancyhead[L]{}}
%	\addtocontents{lol}{\protect\fancyhead[R]{\protect\thepage}}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\phantomsection\addcontentsline{toc}{chapter}{LIST OF LISTINGS}
	\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
	\chapter*{LIST OF LISTINGS}
	\vskip -22\p@  
	\hfill Page
	\vskip 22\p@
	\@starttoc{lol}%
}

\renewcommand*\l@lstlisting[2]{%
	    \addpenalty{-\@highpenalty}%
		\setlength{\@tempdima}{2.5em}
	    \begingroup
	       \parindent \z@ \rightskip \@pnumwidth
	       \parfillskip -\@pnumwidth
	       \leavevmode
	       \advance\leftskip 5.4em% NEW: comment out if no indentation required for lines 2,3,...
	       \hskip -\leftskip
		   \lstlistingname~#1\nobreak\hfil\nobreak
     	   \leaders\hbox{$\m@th
           \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
           mu$}\hfill
		   \hb@xt@\@pnumwidth{\hss #2}\par
	       \penalty\@highpenalty
	    \endgroup
}

%---------------------------------------------------
%	Modified \caption to write lof and lot with ":" 
%---------------------------------------------------
\long\def\@caption#1[#2]#3{%
	\par
    \addcontentsline{\csname ext@#1\endcsname}{#1}%
%	{\protect\numberline{\csname the#1\endcsname:{}\relax}{\ignorespaces #2}}%	change caption format in ToC
	{\protect\numberline{\csname the#1\endcsname\relax}{\ignorespaces #2}}%
	\begingroup
	\@parboxrestore
    \if@minipage
	    \@setminipage
    \fi
    \normalsize
    \@makecaption{\csname fnum@#1\endcsname}{\ignorespaces #3}\par
\endgroup}


%---------------------------------------------------
% Change font size of chapter* headings
%---------------------------------------------------
\def\@makeschapterhead#1{%
  {\parindent \z@ \centering
    \interlinepenalty\@M
	\bfseries \texorpdfstring{\MakeUppercase{\titlefont{#1}}}{}\par\nobreak
  \vskip 40\p@
  }
}

%---------------------------------------------------
% Change font size of chapter headings
%---------------------------------------------------
\def\@makechapterhead#1{%
  \vspace*{17\p@}%
  {\parindent \z@ \centering \normalfont
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
			  \bfseries{\titlefont{CHAPTER\space \thechapter}}\par\nobreak
      \else
			  \bfseries{\titlefont{CHAPTER\space \thechapter}}\par\nobreak
      \fi
    \fi
    \interlinepenalty\@M
	\bfseries \texorpdfstring{\MakeUppercase{\titlefont{#1}}}{}\par\nobreak
    \vskip 30\p@
  }
}

%---------------------------------------------------
%	Patching \bibliography command 
%---------------------------------------------------
\let\oldbibliography\bibliography
\renewcommand{\bibliography}[1]{%
		\fancyhead[L]{\ifthenelse{\isodd{\value{page}}}{\header{Fac. of Grad. Studies, Mahidol Univ.}}{\header\thesisAuthorsInitials}} %odd page header
		\fancyhead[R]{\ifthenelse{\isodd{\value{page}}}{\header{\DegreeAbb~(\SubDegree)~/~\thepage}}{\header{References~/~\thepage}}} %even page header
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
\addtocontents{toc}{\protect\renewcommand{\protect\@dotsep}{400}}
\addcontentsline{toc}{chapter}{\bfseries REFERENCES}
\vspace*{18pt}
\renewcommand\bibname{REFERENCES}
\oldbibliography{#1}
}

%---------------------------------------------------
% Change section headings style
%---------------------------------------------------
%\renewcommand{\section}[1]{\@startsection{section}{1}{\z@}%
%    {-1.5ex \@plus -1ex \@minus -.2ex}%
%    {0.8ex \@plus.2ex}%
%	{\bfseries}[\texorpdfstring{\MakeUppercase{#1}}{}]{\bfseries#1}}

%\renewcommand{\subsection}[1]{\@startsection{subsection}{2}{\z@}%
%	{-1.5ex \@plus -1ex \@minus -.2ex}%
%    {0.8ex \@plus.2ex}%
%	{\bfseries\leftskip 5.5ex}[\texorpdfstring{\MakeUppercase{#1}}{}]{\bfseries#1}}

% End TeX Commands


%---------------------------------------------------
% Setting caption alignment 
%---------------------------------------------------
\captionsetup[figure]{justification=centering}
\captionsetup[table]{position=top,skip=6pt,justification=raggedright,singlelinecheck=off}
\DeclareCaptionFormat{listing} {
	\hspace{-0.2cm}\parbox{\textwidth}{#1#2#3}}
\captionsetup[lstlisting]{format=listing}
%================================================

\makeatother

